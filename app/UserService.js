'use strict';

angular
	.module('users')
	.service('userService', ['$q', UserService]);

function UserService($q){
	var users = [
		{
			name: 'Lia Lugo',
			avatar: 'android',
			content: 'I love cheese'
		},
		{
			name: 'George Duke',
			avatar: 'face',
			content: 'Lorem Ipsum'
		},
	];

	return {
      loadAllUsers : function() {
        // Simulate async nature of real remote calls
        return $q.when(users);
      }
    };
}