'use strict';

// Declare app level module which depends on views, and components
angular
	.module('myApp', [
	  'ngRoute',
	  'ngMaterial',
	  'users'
	])
	.config(function($mdThemingProvider){
		$mdThemingProvider.theme('default').primaryPalette('brown').accentPalette('red');
	})
	.run(function(){
      console.log('MyApp is ready');
    });
