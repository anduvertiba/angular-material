(function(){
	angular
		.module('users')
		.controller('UserController', UserController);

	function UserController(userService, $mdBottomSheet, $mdSidenav) {
		var self = this;

		self.selected = null;
		self.users = [];
		self.selectUser = selectUser;
		self.toggleList = toggleUserList;
		self.share = share;

		userService
			.loadAllUsers()
			.then(function(users){
				self.users = [].concat(users);
				self.selected = users[0];
			});

		function selectUser(user){
			self.selected = angular.isNumber(user) ? self.users[user] : user;
		}

		function toggleUserList() {
			$mdSidenav('left').toggle();
		}

		function share(selectedUser){
			$mdBottomSheet.show({
				controller: UserSheetController,
				controllerAs: 'vm',
				templateUrl: './bottomsheet.html',
				parent: angular.element(document.querySelector('#content'))
			});

			function UserSheetController() {
				this.user = selectedUser;
				this.items = [
					{ name: 'Phone'		, icon: 'phone'},
					{ name: 'Twitter'	, icon: 'phone'},
					{ name: 'Google+'	, icon: 'phone'},
					{ name: 'Hangout'	, icon: 'phone'},
				];
				this.performAction = function(action){
					$mdBottomSheet.hide();
				}
				
			}
		}
	}
})();